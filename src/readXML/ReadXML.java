/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readXML;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author bigma
 */
public class ReadXML {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            File f = new File("src/readXML/simple.xml");
            Document doc = builder.parse(f);
            //lấy node gốc của xml "breakfast_menu"
            Element breakfast = doc.getDocumentElement();
            System.out.println(breakfast.getNodeName());
            //lấy node "food"
            NodeList foods = doc.getElementsByTagName("food");
            for (int i = 0; i < foods.getLength(); i++) {
                Node food = foods.item(i);
                System.out.println("\t<"+food.getNodeName()+">");
                //lấy các phần tử của node "food"
                NodeList nodeList = food.getChildNodes();
                for (int j = 0; j < nodeList.getLength(); j++) {
                    Node node = nodeList.item(j);
                    if (!node.getNodeName().equals("#text")) {    
                        System.out.print("\t\t<"+node.getNodeName()+">");
                        System.out.print(node.getTextContent());
                        System.out.println("</"+node.getNodeName()+">");
                    } 
                }
                System.out.println("\t</"+food.getNodeName()+">");
            }

            System.out.println(breakfast.getNodeName());
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(ReadXML.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
