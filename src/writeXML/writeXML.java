/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package writeXML;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 *
 * @author bigma
 */
public class writeXML {

    public static void main(String[] args) {
        FileOutputStream fileout = null;
        try {
            XMLOutputFactory factory = XMLOutputFactory.newFactory();
            File f = new File("src/writeXML/student.xml");
            fileout = new FileOutputStream(f);
            XMLStreamWriter writer = factory.createXMLStreamWriter(fileout);
            //ghi dữ liệu
            writer.writeStartDocument();
            writer.writeStartElement("Class");
            //student
            for (int i = 0; i < 5; i++) {
                writer.writeStartElement("Student");

                writer.writeStartElement("Name");
                writer.writeCharacters("Le Thi Na" + " " + i);
                writer.writeEndElement();

                writer.writeStartElement("Age");
                writer.writeCharacters("18"+i);
                writer.writeEndElement();

                writer.writeStartElement("Address");
                writer.writeCharacters("Ha Noi"+" "+i);
                writer.writeEndElement();

                writer.writeStartElement("Phone");
                writer.writeCharacters("0123456789"+i);
                
                writer.writeEndElement();
                writer.writeEndElement();
            }
            writer.writeEndDocument();
        } catch (FileNotFoundException | XMLStreamException ex) {
            Logger.getLogger(writeXML.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileout.close();
            } catch (IOException ex) {
                Logger.getLogger(writeXML.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
